# pico-pytest

A minimal re-implementation of pytest core functionality for educational purposes.

## talk versions

Half an hour turns out to be surprisingly short, which is why the code in the notebook is even simpler than what currently lives in the repo. The notebook and everything necessary to run it is in the `talk` folder. 


### pycon.de

**materials for [the Pycon.de 2019 talk](https://youtu.be/zHpeMTJsBRk).**

### OctopusCon in a binder

I gave a slightly extended version of the talk at 
[OctopusCon](https://octopuscon.com/events/octopuscon-python-edition/) and to 
try out how this works put the whole thing binder folder with its own Dockerfile 
to run in python3.8. - that one is is currently in a different 
repo [here](https://github.com/obestwalter/abridged-meta-programming-classics).


## more comprehensive versions as complete package

* [X] ([tag 2.0+collection](https://gitlab.com/obestwalter/pico-pytest/tree/2.0+collection)) recursive test collection from `cwd` down
* [X] ([tag 2.1+execution](https://gitlab.com/obestwalter/pico-pytest/tree/2.1+execution)) test execution
* [X] ([tag 2.2+marking](https://gitlab.com/obestwalter/pico-pytest/tree/2.2+marking)) test selection via markers and marker expressions
* [X] ([tag 2.3+fixtures](https://gitlab.com/obestwalter/pico-pytest/tree/2.3+fixtures)) automatic dependency injection via test fixtures

## try it out

with tox:

    $ tox -e pico-pytest

manually:

Clone this repo and cd into the clone:

```console
# create venv
$ python3.6 -m venv .venv

# activate it 
# (posix)
$ source .venv/bin/activate
# (Windows)
# .venv/Scripts/activate.bat

# install pico-pytest in development mode
$ pip install -e .

# enter the directory with the test demo "suite"
$ cd demo
```

run pico-pytest:

```console
$ pico-pytest
pico-pytest (fixtures) working in: /home/ob/do/pico-pytest/demo/pico_pytest_fixtures
F.

FAILURES:
test_pico_pytest_failing_automatic_fixture_injection: AssertionError: ()

executed: 2, failed: 1
```

## run the tests with [tox](https://tox.readthedocs.io):

    $ tox
