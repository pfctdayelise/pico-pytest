import shlex
import subprocess
from pathlib import Path

import pytest


class Config:
    COMMAND = ["pico-pytest"]

    def __init__(
        self,
        name,
        *,
        args: str = None,
        allTests=True,
        ret=0,
        includes=None,
        excludes=None,
    ):
        self.name = name
        self.command = self.COMMAND + shlex.split(args) if args else self.COMMAND
        self.allTests = allTests
        self.ret = ret
        self.includes = includes or []
        self.excludes = excludes or []

    def __repr__(self):
        return f"{self.__class__.__name__}\n" + "\n".join(
            [f"\t{k}='{v}'" for k, v in self.__dict__.items() if not k.startswith("_")]
        )


configs = (
    Config(
        "collection: finds all tests",
        args="--collect-only",
        includes=[
            "collected 11 tests",
            "function test_passes",
            "function test_passes_with_two_markers",
            "function test_spanish",
            "function test_inquisition",
        ],
    ),
    Config(
        "collection: respects mark expression",
        args="--collect-only -m ebony",
        includes=["collected 2 tests"],
    ),
    Config(
        "simple run: no errors",
        allTests=False,
        includes=["..\n", "executed: 2", "all is fine"],
    ),
    Config(
        "simple run: with errors",
        ret=1,
        includes=[
            "test_eggs: FileNotFoundError",
            "test_fails_with_syntax_error: SyntaxError('please try again')",
            "test_pytest_failing_automatic_fixture_injection: AssertionError",
            "test_pico_pytest_failing_automatic_fixture_injection: AssertionError",
            "executed: 11, failed: 4",
        ],
    ),
)


@pytest.mark.parametrize("config", configs, ids=lambda config: config.name)
def test_collect_only(
    monkeypatch, capfd, allTests: Path, passingTests: Path, config: Config
):
    monkeypatch.chdir(allTests if config.allTests else passingTests)
    assert subprocess.run(config.command).returncode == config.ret
    std = capfd.readouterr()
    assert not std.err
    for include in config.includes:
        assert include in std.out, f"\n'{include}' not in\n{std.out}"
    for exclude in config.excludes:
        assert exclude not in std.out, f"\n'{exclude}' in\n{std.out}"
