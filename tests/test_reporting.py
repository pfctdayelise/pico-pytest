from pico_pytest.reporting import display_collected, report


def test_display_collected(capsys):
    tests = [lambda: None, lambda: None, lambda: None, lambda: None]
    display_collected(tests)
    captured = capsys.readouterr()
    assert "collected 4 tests" in captured.out
    assert "collected 4 tests" in captured.out
    assert "<function <lambda>" in captured.out


def test_display_collected_failed(capsys):
    tests = {lambda: 0: "passed", lambda: 0: ValueError("BOOM!")}
    ret = report(tests)
    assert ret == 1
    captured = capsys.readouterr()
    assert "FAILURES:" in captured.out
    assert "ValueError('BOOM!')" in captured.out


def test_display_collected_passed(capsys):
    tests = {lambda: 0: "passed", lambda: 0: "passed"}
    ret = report(tests)
    assert ret == 0
    captured = capsys.readouterr()
    assert "FAILURES:" not in captured.out
    assert "all is fine" in captured.out
