from pathlib import Path

import pytest

from pico_pytest.collection import get_module, collect_test_paths

PATH_TO_DEMO = Path(__file__).parents[1] / "demo"
PATH_TO_PASSING_TESTS = PATH_TO_DEMO / "all_passing"


@pytest.fixture(scope="session", name="allTests")
def provide_path_to_all_test_modules():
    return PATH_TO_DEMO


@pytest.fixture(scope="session", name="passingTests")
def provide_path_to_only_passing_tests():
    return PATH_TO_PASSING_TESTS


@pytest.hookimpl
def pytest_generate_tests(metafunc):
    if "module" in metafunc.fixturenames:
        paths = collect_test_paths(PATH_TO_DEMO)
        modules = [get_module(path) for path in paths]
        metafunc.parametrize("module", modules, ids=lambda m: m.__name__)
