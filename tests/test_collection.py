from types import ModuleType, FunctionType

from pico_pytest.collection import (
    collect,
    get_module,
    collect_test_paths,
    collect_test_functions,
)


def test_collect(allTests):
    tests = collect(allTests)
    assert len(tests) == 11
    functionNames = [t.__name__ for t in tests]
    testNames = ["test_passes", "test_spam", "test_inquisition"]
    assert all(n in functionNames for n in testNames)


def test_find_test_paths(allTests):
    testPaths = collect_test_paths(allTests)
    assert len(testPaths) == 5
    assert all(tp.exists() for tp in testPaths)
    assert all(tp.stem.startswith("test_") for tp in testPaths)


def test_create_test_module(allTests):
    module = get_module(allTests / "test_one.py")
    assert isinstance(module, ModuleType)
    assert hasattr(module, "test_passes")


def test_create_test_module_needs_to_be_executed(monkeypatch, allTests):
    """Demonstrate that when `exec_module` needs to be called on loader."""
    from importlib._bootstrap_external import _LoaderBasics

    monkeypatch.setattr(_LoaderBasics, "exec_module", lambda *_: None)
    module = get_module(allTests / "test_one.py")
    assert isinstance(module, ModuleType)
    assert not hasattr(module, "test_ebony")


def test_fetch_functions(module):
    functions = collect_test_functions(module)
    for function in functions:
        assert isinstance(function, FunctionType)
        assert function.__name__.startswith("test_")
