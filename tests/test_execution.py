from pico_pytest.execution import execute_test
from pico_pytest.fixtures import PicoPytestFixture, name2fixture


def test_execute_passing_test():
    def test_answer():
        assert 42 == 42

    assert execute_test(test_answer) == "passed"


def test_execute_failing_test():
    def test_answer():
        assert 23 == 42

    e = execute_test(test_answer)
    assert isinstance(e, AssertionError)
    assert "assert 23 == 42" in e.args[0]


def test_execute_passing_test_with_fixture(monkeypatch):
    def test_answer(the_answer):
        assert the_answer == 42

    def the_answer():
        return 42

    monkeypatch.setitem(name2fixture, "the_answer", PicoPytestFixture(the_answer))
    assert execute_test(test_answer) == "passed"


def test_execute_failing_test_with_fixture(monkeypatch):
    def test_answer(the_answer):
        assert the_answer == 42

    def the_answer():
        return 23

    monkeypatch.setitem(name2fixture, "the_answer", PicoPytestFixture(the_answer))
    e = execute_test(test_answer)
    assert isinstance(e, AssertionError)
    assert "assert 23 == 42" in e.args[0]
