from pico_pytest import main
from pico_pytest.fixtures import fixture, PicoPytestFixture, name2fixture

TEST_TEXT = "test text"


def check_fixture(fixture, autouse=False):
    assert isinstance(fixture, PicoPytestFixture)
    assert str(fixture).startswith(f"PicoPytestFixture")
    assert fixture.autouse == autouse
    assert fixture.function() == TEST_TEXT


def test_decorated_without_parenthesis():
    @fixture
    def spam():
        return TEST_TEXT

    check_fixture(spam)


def test_decorated_with_parenthesis():
    @fixture()
    def spam():
        return TEST_TEXT

    assert "spam" in name2fixture
    check_fixture(spam)


def test_different_name():
    name = "i_am_different"

    @fixture(name=name)
    def different_name():
        return TEST_TEXT

    assert name in name2fixture
    check_fixture(different_name)


def test_different_autouse_setting():
    @fixture(autouse=True)
    def autouse_fixture():
        return TEST_TEXT

    check_fixture(autouse_fixture, autouse=True)


def test_fixture_run(monkeypatch, capsys, allTests):
    monkeypatch.chdir(allTests / "pico_pytest_fixtures")
    main(args=[])
    captured = capsys.readouterr()
    assert "executed: 2, failed: 1" in captured.out
    assert (
        "FAILURES:\n"
        "test_pico_pytest_failing_automatic_fixture_injection: "
        "AssertionError"
    ) in captured.out
