from types import FunctionType
from typing import List, Tuple

import pytest

import pico_pytest
from pico_pytest.marking import filter_tests, MarkMapping


def test_simple_mark():
    @pico_pytest.mark.simple
    def spam(txt):
        return txt

    check_marked_function(spam, expected_markers=["simple"])


def test_more_than_one_mark():
    @pico_pytest.mark.last
    @pico_pytest.mark.second
    @pico_pytest.mark.first
    def spam(txt):
        return txt

    check_marked_function(spam, expected_markers=["first", "second", "last"])


def check_marked_function(func: FunctionType, expected_markers: List[str]):
    assert isinstance(func, FunctionType)
    markers = getattr(func, "markers")
    assert markers == expected_markers
    test_text = "test text"
    assert func(test_text) == test_text


@pytest.mark.parametrize(
    "expression, markers, expectation",
    (
        ("spam", ["spam"], True),
        ("not spam", ["spam"], False),
        ("not spam", ["eggs"], True),
        ("spam or eggs", ["unknown"], False),
        ("spam or eggs", ["spam"], True),
        ("spam or eggs", ["eggs"], True),
        ("spam or eggs", ["spam", "eggs"], True),
        ("spam and eggs", ["spam", "eggs"], True),
        ("spam and eggs", ["spam"], False),
        ("spam and eggs", ["eggs"], False),
        ("spam and eggs", ["unknown"], False),
    ),
    ids=lambda params: str(params),
)
def test_fits_marker_expression(expression: str, markers: List[str], expectation: bool):
    assert eval(expression, {}, MarkMapping(markers)) == expectation


@pytest.mark.parametrize(
    "expression, test_definitions, expectation",
    (
        ("spam", [("a", ["spam", "eggs"])], ["a"]),
        ("not spam", [("a", ["spam", "eggs"])], []),
        ("not spam", [("a", []), ("b", ["eggs"]), ("c", ["spam"])], ["a", "b"]),
        ("x", [("a", ["x", "y"]), ("b", ["y"])], ["a"]),
        ("y", [("a", ["x", "y"]), ("b", ["y"])], ["a", "b"]),
        ("x", [("a", ["x", "y"]), ("b", ["x"])], ["a", "b"]),
        ("x and y", [("a", ["x", "y"]), ("b", ["y"])], ["a"]),
        ("x or y", [("a", ["x", "y"]), ("b", ["y"])], ["a", "b"]),
        ("x and not y", [("a", ["x", "y"]), ("b", ["x"])], ["b"]),
    ),
    ids=lambda params: str(params),
)
def test_filter_tests_by_marker_expression(
    expression: str, test_definitions: Tuple[str, List[str]], expectation: List[str]
):
    class FunctionType:  # because why not :)
        def __init__(self, name, markers):
            self.name = name
            self.markers = markers

    fake_tests = [FunctionType(name, markers) for name, markers in test_definitions]
    filtered_tests = filter_tests(fake_tests, expression)
    assert len(filtered_tests) == len(expectation)
    filtered_test_names = [name for name, _ in test_definitions]
    for test in filtered_tests:
        assert test.name in filtered_test_names  # noqa
