# notebook used in the talk

The code on the slides is a bit further simplified compared to the code in the repository. 

## run notebook directly

If you have Python3.8 with jupyter and pytest installed, run in this directory:

    jupyter notebook --no-mathjax abridged-classics-pytest.ipynb
    
## run notebook in tox env:

If you have tox installed and want to run the notebook in its own env run in this directory:

    $ tox -e serve
    
## run notebook in Docker container with tox in this directory:

    $ tox -e docker-build
    
followed by 

    $tox -e docker-run

    tox -e docker-run

    [...]
    docker-run run-test: commands[3] | docker exec -it amc jupyter notebook list
    Currently running servers:
    http://0.0.0.0:8888/?token=07427f868452a0a85a1cf0a64fd6f0358383906665e6dd73 :: /talk

Copy the url after `currently running servers:` into the address bar of your browser to access the notebook.

To tidy up run `tox -e docker-stop` (optionally) followed by `tox -e docker-clean` 

Have fun!
