import pytest
import pico_pytest


@pytest.mark.ebony
@pico_pytest.mark.ebony
def test_passes():
    pass


@pytest.mark.ivory
@pico_pytest.mark.ivory
def test_fails_with_syntax_error():
    raise SyntaxError("please try again")


@pytest.mark.ebony
@pytest.mark.ivory
@pico_pytest.mark.ebony
@pico_pytest.mark.ivory
def test_passes_with_two_markers():
    pass


def non_test_function():
    assert 0, "this should have never been called by the test framework"
