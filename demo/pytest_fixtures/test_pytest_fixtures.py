import pytest


@pytest.fixture(name="the_answer")
def find_the_answer():
    return 42


def test_pytest_passing_automatic_fixture_injection(the_answer):
    assert the_answer == 42


def test_pytest_failing_automatic_fixture_injection(the_answer):
    assert the_answer == 23
