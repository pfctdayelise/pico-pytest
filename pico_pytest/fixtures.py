"""Automatic dependency injection with fixture functions and parameter inspection.

pytest docs: https://docs.pytest.org/en/latest/fixture.html

pytest sources: src/_pytest/fixtures.py

NOTE: interchangeable decoration with and without parentheses has also found it way
      to stdlib: https://bugs.python.org/issue36772
"""
from types import FunctionType
from typing import Optional, Union

name2fixture = {}


def fixture(
    name: Optional[Union[FunctionType, str]] = None, autouse: bool = False
) -> "PicoPytestFixture":
    return PicoPytestFixture(name=name, autouse=autouse)


class PicoPytestFixture:
    def __init__(
        self, name: Union[None, str, FunctionType] = None, autouse=False
    ) -> None:
        self._name = name
        self.autouse = autouse
        if isinstance(name, FunctionType):
            self._register_function(function=name, name=name.__name__)

    def __str__(self) -> str:
        return f"{self.__class__.__name__}\n" + "\n".join(
            [f"\t{k}='{v}'" for k, v in self.__dict__.items() if not k.startswith("_")]
        )

    __repr__ = __str__

    def __call__(self, function) -> "PicoPytestFixture":
        self._register_function(function)
        return self

    def _register_function(self, function: FunctionType, name=None) -> None:
        self.function = function
        name2fixture[name or self._name or function.__name__] = self
