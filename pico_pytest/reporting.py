"""Print information about collected tests and test outcome.

pytest sources: src/_pytest/reports.py
"""
from types import FunctionType
from typing import Dict, List, Union


def display_collected(tests: List[FunctionType]):
    print(f"collected {len(tests)} tests:")
    for test in tests:
        print(f"\t<{type(test).__name__} {test.__name__}>")


def report(results: Dict[FunctionType, Union[str, Exception]]) -> int:
    failed = {
        test: result for test, result in results.items() if not isinstance(result, str)
    }
    if failed:
        print("\n\nFAILURES:")
        for test, e in failed.items():
            msg = e.args[0] if len(e.args) else "<no assertion information>"
            result = f"{e.__class__.__name__}('{msg}')"
            print(f"{test.__name__}: {result}")
        print(f"\nexecuted: {len(results)}, failed: {len(failed)}")
    else:
        print(f"\nexecuted: {len(results)} (all is fine!)")
    return 1 if failed else 0
