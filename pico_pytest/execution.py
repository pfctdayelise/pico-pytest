"""Test exection.

pytest sources: src/_pytest/pythonöpy (pytest_pyfunc_call)
"""
import inspect
from pathlib import Path
from types import FunctionType
from typing import Dict, List, Union

from pico_pytest.collection import collect
from pico_pytest.fixtures import name2fixture
from pico_pytest.marking import filter_tests
from pico_pytest.reporting import report, display_collected


def pico_pytest(path: Path, *, collectOnly: bool, markExpression: str) -> int:
    for fixture in [f for f in name2fixture.values() if f.autouse]:
        fixture.function()
    tests = collect(path)
    if markExpression:
        tests = filter_tests(tests, markExpression)
    if collectOnly:
        display_collected(tests)
        return 0
    results = execute_tests(tests)
    return report(results)


def execute_tests(
    functions: List[FunctionType]
) -> Dict[FunctionType, Union[str, Exception]]:
    """Execute all tests and collect the results in a dictionary."""
    return {function: execute_test(function) for function in functions}


def execute_test(function: FunctionType) -> Union[str, Exception]:
    requestedFixtures = [name for name in inspect.signature(function).parameters.keys()]
    kwargs = {name: name2fixture[name].function() for name in requestedFixtures}
    result = "."
    try:
        function(**kwargs)
        return "passed"
    except Exception as e:
        result = "F"
        return e
    finally:
        print(result, end="")
