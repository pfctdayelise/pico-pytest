"""Command line interface.

pytest docs: http://pytest.org/en/latest/usage.html

pytest sources: src/_pytest/config
"""
import argparse
import os
from pathlib import Path

from pico_pytest.execution import pico_pytest


def main(args=None) -> int:
    args = parse_args(args)
    searchPath = Path(args.search_path)
    print(f"pico-pytest lookinkg for tests in in: {searchPath}")
    return pico_pytest(searchPath, collectOnly=args.collect_only, markExpression=args.m)


def parse_args(args=None):
    parser = argparse.ArgumentParser("pico-pytest")
    parser.add_argument(
        "--collect-only",
        action="store_true",
        help="only collect tests and display them.",
    )
    parser.add_argument("-m", help="expression to choose tests via markers")
    parser.add_argument("search_path", nargs="?", default=os.getcwd())
    return parser.parse_args(args)
