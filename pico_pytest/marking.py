"""Mark functions with arbitrarily names and them tests via expressions.

pytest docs: https://docs.pytest.org/en/latest/mark.html

pytest sources: src/_pytest/mark
"""
from types import FunctionType
from typing import List, Callable


class MarkAttacherFactoryFactory:
    def __getattr__(self, name: str) -> Callable:
        return self.mark_attacher_factory(name)

    @staticmethod
    def mark_attacher_factory(name: str) -> Callable:
        def inner(func) -> Callable:
            try:
                func.markers.append(name)
            except AttributeError:
                func.markers = [name]
            return func

        return inner


mark = MarkAttacherFactoryFactory()


def filter_tests(tests: List[FunctionType], expression: str) -> List[FunctionType]:
    filteredTests = []
    for test in tests:
        markers = getattr(test, "markers", [])
        if eval(expression, {}, MarkMapping(markers)):
            filteredTests.append(test)
    testCount = len(tests)
    filteredTestCount = len(filteredTests)
    if testCount != filteredTestCount:
        print(f"skipping {testCount - filteredTestCount} tests.")
    return filteredTests


class MarkMapping(dict):
    # inherit from dict to make mypy happy - is there a better way?
    def __init__(self, marks: List[str]):  # noqa
        self.marks = marks

    def __getitem__(self, mark: str) -> bool:
        return mark in self.marks
