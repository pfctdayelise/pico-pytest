"""Dynamically import modules and collect the test functions.

pytest sources: src/_pytest/python.py (PyCollector, Module)

python docs: https://docs.python.org/3/reference/datamodel.html
"""
import importlib.util
from itertools import chain
from pathlib import Path
from types import ModuleType, FunctionType
from typing import List


def collect(path: Path) -> List[FunctionType]:
    """Given a path: discover all test functions in all test modules."""
    paths = collect_test_paths(path)
    modules = [get_module(path) for path in paths]
    tests = list(chain(*[collect_test_functions(module) for module in modules]))
    return tests


def collect_test_paths(path: Path) -> List[Path]:
    """Find all Python files recursively in search_path with prefix 'test_'."""
    return list(path.glob("**/test_*.py"))


def get_module(path: Path) -> ModuleType:
    """Given a path load a Python module and evaluate it."""
    spec = importlib.util.spec_from_file_location(path.stem, path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)  # type: ignore
    return module


def collect_test_functions(module: ModuleType) -> List[FunctionType]:
    """Given a module: collect all test functions from it."""
    functions = []
    for name in [n for n in dir(module) if n.startswith("test_")]:
        obj = getattr(module, name)
        if isinstance(obj, FunctionType):
            functions.append(obj)
    return functions
